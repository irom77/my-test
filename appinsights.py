# from applicationinsights import TelemetryClient

import socket
socket.setdefaulttimeout(30)

from applicationinsights import TelemetryClient
tc = TelemetryClient('')
tc.track_event('Test event', { 'foo': 'bar' }, { 'baz': 42 })
tc.flush()
"""
from applicationinsights import TelemetryClient
tc = TelemetryClient('')
tc.context.application.ver = '1.2.3'
tc.context.device.id = 'My current device'
tc.context.device.oem_name = 'Asus'
tc.context.device.model = 'X31A'
tc.context.device.type = "Other"
tc.context.user.id = 'santa@northpole.net'
tc.track_trace('My trace with context')
tc.flush()
"""